// 引入vue依赖
import Vue from 'vue'
// 引入路由依赖
import Router from 'vue-router'
// 引入一个测试模版
import HelloWorld from '@/components/HelloWorld'
// 引入Login模版，用于在主模版中插入登录
import Login from '@/components/Login.vue'
import Reg from '@/components/Register.vue'
import MyMain from '@/components/Main.vue'
import Home from '@/components/Home.vue'
import TeacherClass from '@/components/TeacherClass.vue'
import CreateClass from '@/components/CreateClass.vue'
import StudentClass from '@/components/StudentClass.vue'
import AddClass from '@/components/AddClass.vue'
import PushWrok from '@/components/PushWrok.vue'
import AClass from '@/components/AClass.vue'
import StudentWrok from '@/components/StudentWrok.vue'
import CommitWork from '@/components/CommitWork.vue'

// 引入状态管理，用于验证等
import store from '../store/index.js'
// 防止多次注册
Vue.use(Router)
// 暴露一个路由实例
let beforeEach = true
const router = new Router({

	routes: [{
			path: '/login',
			name: 'Login',
			component: Login
		},
		{
			path: '/reg',
			name: 'Reg',
			component: Reg
		},
		{
			path: '/helloworld',
			name: 'HelloWorld',
			component: HelloWorld
		},

		{
			path: '/home',
			name: 'Home',
			component: Home,
			children: [{
					path: 'teacherclass',
					name: 'TeacherClass',
					component: TeacherClass
				},
				{
					path: 'createclass',
					name: 'CreateClass',
					component: CreateClass
				},
				{
					path: 'studentclass',
					name: 'StudentClass',
					component: StudentClass

				},

				{
					path: 'addclass',
					name: 'AddClass',
					component: AddClass

				},
				{
					path: 'pushwork/:id',
					name: 'PushWrok',
					component: PushWrok

				},
				{
					path: 'aclass/:id',
					name: 'AClass',
					component: AClass,
					children:[{
						// 不写可以，也可以获取到父id
						path: 'studentwrok/:id',
						name: 'StudentWrok',
						component: StudentWrok,
					},
					{
						// 不写可以，也可以获取到父id
						path: 'commitwork/:classId/:workId',
						name: 'CommitWork',
						component: CommitWork,
					}
					]
				
				}






			]

		}
	]
})

//添加路由守卫
if (beforeEach) {
	router.beforeEach((to, from, next) => {

		console.log("触发路由守卫" + store.getters.getLogin)
		console.log(to.path)
		console.log(from.path)
		console.log(next.path)

		if (!store.getters.getLogin && to.path != "/login" && to.path != "/reg") {

			next("/login")
		} else {
			//  next({ name: 'Login' }
			next()
		}
	})

}


export default router
