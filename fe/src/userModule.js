

//全局state对象，用于保存所有组件的公共数据
const state={
  //定义一个user对象
  //<span>{{$store.state.user.student}}</span><br/><br/>
  user:{
    name:'user',
  },
  student:{
    id:'21321313'
  }
}

/* 监听state对象的值的最新状态 （计算属性）*/
const getters={
  //定义一个对象，通过state对象获得user
  getUser(state){
    return state.user;
  }

}

//唯一一个可以修改state值的方法(同步方法，会阻塞)
const mutations={
  updateUser(state,user){
    state.user = user;
  }
}

//异步执行mutations方法

const actions={
  // context原指上下文，现在指  mutations
  asyncUpdateUser(context,user){
    context.commit('updateUser',user);
  }
}


//导出模块
export default {
    namespaced: true,

    state,
    getters,
    mutations,
    actions

  }
