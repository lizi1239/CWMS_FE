// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
// 引入vue实例依赖
import Vue from 'vue'
// 引入挂载到根元素实例需要的模版
import App from './App.vue'
// 引入路由实例，在./router/index.js中实现
import router from './router'
// 引入store实例，在./store/index.js中实现
import store from './store/index.js'
// 引入饿了么ui的依赖
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
// axios
import Axios from 'axios'
Vue.config.productionTip = false



Vue.prototype.Axios = Axios
Vue.use(ElementUI)
/* eslint-disable no-new */
// *
//  * 在app根元素上挂载了vue实例


new Vue({
	// 挂载的根元素
  el: '#app',
  // 路由实例，子组件可以通过 this.$router访问到
  router,
  // 状态管理实例，子组件可以通过 this.$store访问到
  store,
  // 注册的模版,必须先注册，后面再引用
  components: { App },
  //挂载到根元素的模版
  template: '<App/>'
})
