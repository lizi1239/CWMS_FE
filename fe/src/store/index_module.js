import Vue from 'vue';
import Vuex from 'vuex'
import userModule from './userModule.js'

Vue.use(Vuex);


export default new Vuex.Store(
  {
  modules: { // 这里声明模块
    userModule
  }
  }
  )
