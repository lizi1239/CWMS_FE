// 引入vue依赖
import Vue from 'vue'
// 引入vuex依赖
import Vuex from "vuex"

// 防止多次注册
Vue.use(Vuex)
// 暴露一个store实例


// 当状态过多,管理混乱,需要引入模块
export default new Vuex.Store({
	// 状态s
	state: {
		test: "这是一个测试用的状态",
		login: false,
		server:{
			host:"127.0.0.1",
			// host:"121.4.185.202",
			
			port:"8081"
		},
		user:{
			   account:"",
			    userName:"",
			    msg:"",
			    code:"",
			    data:"",
			    token:""
		},
		debug:true,
		type:"",
		studentClassData:[],
		teacherClassData:[],
		pushWorkData:[],
	},

	//获取状态的标准方法
	getters: {
		getPushWorkData: state => {
			return state.pushWorkData
		},
		getLogin: state => {
			return state.login
		},
		getTest(state){
		  return state.test;
		},
		
		getServer(state){
		  return state.server;
		}
		,
		
		getUser(state){
		  return state.user;
		},
		getDebug(state){
		  return state.debug;
		}
		,
		getType(state){
			
		  return state.type;
		},
		getStudentClassData(state){
		  return state.studentClassData;
		},
		getTeacherClassData(state){
			
		  return state.teacherClassData;
		}
		
		
	},


	// 状态改变必须要经过mutations，这里的方法必须是同步的，使用 store的commit可以调用到
	mutations: {
		// 第一个参数是上面定义的state（默认），其他参数是以调用时需要的参数——即载荷
		// 载荷一般是对象
		// 调用方法: store.commit("setTest","一个新的test状态")
		setPushWorkData: (state, str) => {
			
			state.pushWorkData = str
		},
		setTeacherClassData: (state, str) => {
			
			state.teacherClassData = str
		},
		setStudentClassData: (state, str) => {
			
			state.studentClassData = str
		},
		setTest: (state, str) => {
			state.test = str
		},
		setLogin: (state, bol) => {
			state.login = bol
		},
		setUser: (state, bol) => {
			state.user = bol
			// state.user.account = bol.account
			    // userName:"",
			    // msg:"",
			    // code:"",
			    // data:"",
			    // token:""
		},
		setType: (state, bol) => {
			state.type = bol
		}
	
	},

	//改变状态的异步方法，必须调用mutations
	actions: {
		// context 提供了 store的commint相同方法，可以提交commit请求
		asyncSetTest: (context, str) => {
			context.commit("setTest", str)
		},
		asyncSetLogin: (context, bol) => {
			context.commit("setLogin", bol)
		},
		
		asyncSetUser: (context, bol) => {
			context.commit("setUser", bol)
		}
	}
})
